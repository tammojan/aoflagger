#ifndef AOFLAGGER_STRUCTURES_H
#define AOFLAGGER_STRUCTURES_H

#include "../structures/image2d.h"
#include "../structures/mask2d.h"

#include "../quality/histogramcollection.h"
#include "../quality/statisticscollection.h"

#include <vector>

namespace aoflagger {

class FlagMaskData {
public:
	explicit FlagMaskData(Mask2DPtr theMask) : mask(theMask)
	{
	}
	
	Mask2DPtr mask;
};

class ImageSetData {
public:
	explicit ImageSetData(size_t initialSize) : images(initialSize)
	{
	}
	
	std::vector<Image2DPtr> images;
};

class QualityStatisticsDataImp
{
public:
	QualityStatisticsDataImp(const double* _scanTimes, size_t nScans, size_t nPolarizations, bool _computeHistograms) :
		scanTimes(_scanTimes, _scanTimes+nScans),
		statistics(nPolarizations),
		histograms(nPolarizations),
		computeHistograms(_computeHistograms)
	{
	}
	std::vector<double> scanTimes;
	StatisticsCollection statistics;
	HistogramCollection histograms;
	bool computeHistograms;
};

class QualityStatisticsData
{
public:
	QualityStatisticsData(const double* _scanTimes, size_t nScans, size_t nPolarizations, bool computeHistograms) :
		_implementation(new QualityStatisticsDataImp(_scanTimes, nScans, nPolarizations, computeHistograms))
	{
	}
	explicit QualityStatisticsData(std::shared_ptr<QualityStatisticsDataImp> implementation) :
		_implementation(implementation)
	{
	}
	std::shared_ptr<QualityStatisticsDataImp> _implementation;
};

} // namespace aoflagger

#endif
